# Frontend Challenge (Tiago Mendes) - Körber Pharma Software

## Introduction

Dear recruiters,

First of all, I want to sincerely thank you for giving me this opportunity to demonstrate my programming skills through the resolution of this frontend challenge. The Körber company seems a very professional and interesting company, and that is why being able to participate in this interview process is a great honor to me.

I'm not going to lie to you: this was challenging. I had little time to implement all the requested requirements, and maybe with some more extra time I could deliver a more complete and robust solution. However, and in my opinion, I think the final application is quite ok.

Even if I do not pass this interview phase, I already learned a lot with this challenge, and this certainly helped me improve my frontend skills. In that case, I humbly ask you to give me feedback about what I did wrong, so I can also learn from my mistakes. However, I sincerely hope that you like my work, because I genuinely want to pass to the next phase! 🚀

In the following sections, I will try to explain the best I can the approaches that I took to solve different problems.

If you have any doubt regarding my solution, you can contact me through the following channels:

-   **LinkedIn:** [tiagocmendes](https://www.linkedin.com/in/tiagocmendes/)
-   **Email:** tiagomendes@protonmail.com
-   **Phone number:** +351 966 035 286

I hope you enjoy my work! 😄

## TL;DR - Application demo

In order for you to watch my application running, I prepared a small video demonstrating all the requested requirements. You can watch this demonstration video on this [Google Drive link](https://drive.google.com/file/d/1l6s4K_gM97LnctDd48UuRXUW8jILOrMk/view?usp=sharing).

## How to run my solution

### System requirements and libraries

To solve this challenge, I used the following system requirements and libraries:

-   **Operative system:** _macOS Monterey 12.4_
-   **Browser:** _Chrome Browser Version 104.0.5112.79_
-   **NodeJS:** _v16.17.0 (managing it with nvm 0.39.1)_
-   **React:** _v18.2.0_
-   **TypeScript:** _v4.7.4_
-   **MaterialUI:** _v5.10.1_
-   **Auth0-React:** _v1.10.2_

### Time to run!

In order to run my application, you should follow these steps:

1. Clone this repository from GitLab [https://gitlab.com/tiagocm/korber-challenge](https://gitlab.com/tiagocm/korber-challenge)

2. Guarantee that you have **_NodeJS v16.17.0_** installed on your OS (you can download it [here](https://nodejs.org/download/release/latest-v16.x/).) If you already have another version of NodeJS installed on your OS, you can also use [nvm](https://github.com/nvm-sh/nvm) to install **_NodeJS v16.17.0_**.

3. Go to the root of this repository and execute `npm install`. This command should create a [./node_modules](./node_modules/) folder.

4. Execute `npm start`. This will serve my application at [http://localhost:3000](http://localhost:3000).

5. Have fun while exploring my app 😄

Disclaimer: it worked on my machine! 😋

## Implemented requirements

1.  _Login/logout (we provide you an Auth0 sandbox, see below) (Allowed
    Google/Facebook/Windows):_ - ✅

    -   To complete this step, I used the [auth0-react SDK](https://auth0.com/docs/quickstart/spa/react/interactive). In order to authenticate, I used the Google SSO.

2.  _Display our identity (name/photo) in the application:_ - 🟡

    -   This was probably the only point that I was not able to complete entirely. After completing the previous step, every time I got redirected to the [http://localhost:3000/callback](http://localhost:3000/callback), I was not able to retrieve the authentication token (I was receiving a `401 - Unauthorized`). Because of this error, I was not able to display your identity (name/photo) in my application. However, in order to simulate the authentication process, I created an hardcoded user to use to complete the rest of the requirements (protect routes, retrieve the posts from this user, add new posts to this user, etc).

    ```javascript
    setUser({
        userId: 1,
        name: "Cristiano Ronaldo",
        email: window.localStorage.getItem("currentEmail") || "",
    });
    ```

3.  _Show initially an input field where user can type an E-Mail address_ - ✅

4.  _Show a button next to the input field, that continues to step 3 when pressed_ - ✅

5.  _Validate the input and if the input is not an E-Mail address show an error message and stop_ - ✅

6.  _Since this is a known API user, save the E-Mail address so that it's automatically filled in the input field next time the user opens the application_ - ✅

7.  _Get a list of posts for this user (hint: use the user's id with this endpoint [https://jsonplaceholder.typicode.com/posts?userID=USER_ID]([https://jsonplaceholder.typicode.com/posts?userID=USER_ID]))_ - ✅

8.  Create a new post and send it - ✅

9.  Append newly created posts (note: API won't actually save new posts so just add it locally) - ✅

## Technical requirements

-   TypeScript 4.3+ (optional, bonus points) - ✅
-   lint/eslint well configured - ✅ (at [./tsconfig.json](./tsconfig.json))
-   One e2e Test (optional, bonus points) - ❌
-   Use your favorite framework (Vue.js, React.js, Angular) - used [React 18](https://reactjs.org/blog/2022/03/29/react-v18.html) - ✅
-   Use the UI library you want (bootstrap/material/your owned/ etc) - ✅ (used [MaterialUI](https://mui.com/) and also custom components with CSS)

## Author

This work was entirely made by [Tiago Mendes](https://www.linkedin.com/in/tiagocmendes/) ([tiagocmendes](https://github.com/tiagocmendes)).

August 2022
