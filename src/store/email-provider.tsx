import React, { useReducer } from "react";

interface EmailState {
    touched: boolean;
    valid: boolean;
    value: string;
}

interface EmailContextInterface {
    state: EmailState;
    onChangeEmailInput: (email: string) => void;
}

const currentEmail = window.localStorage.getItem("currentEmail");

const validateEmail = (email: string): boolean => {
    const filter =
        /^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return filter.test(email);
};

const initialEmailState = {
    touched: false,
    valid: validateEmail(currentEmail || ""),
    value: currentEmail || "",
};

const initialEmailContext: EmailContextInterface = {
    state: initialEmailState,
    onChangeEmailInput: () => {},
};

export const EmailContext =
    React.createContext<EmailContextInterface>(initialEmailContext);

const emailReducer = (
    state: EmailState,
    action: { type: string; value: string }
): EmailState => {
    switch (action.type) {
        case "VALIDATE":
            const emailIsValid = validateEmail(action.value);

            if (emailIsValid) {
                window.localStorage.setItem("currentEmail", action.value);
            }

            return {
                touched: true,
                valid: emailIsValid,
                value: action.value,
            };
        default:
            return initialEmailState;
    }
};

export const EmailContextProvider: React.FC<{
    children: React.ReactNode;
}> = ({ children }) => {
    const [state, dispatch] = useReducer(emailReducer, initialEmailState);

    const onChangeEmailInput = (email: string) => {
        dispatch({ type: "VALIDATE", value: email });
    };

    return (
        <EmailContext.Provider value={{ state, onChangeEmailInput }}>
            {children}
        </EmailContext.Provider>
    );
};
