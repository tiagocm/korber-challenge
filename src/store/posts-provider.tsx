import React, { useState } from "react";

import Post from "../models/post";

interface PostContextInterface {
    initialPosts: boolean;
    posts: Post[];
    setInitialPostsHandler: (post: Post[]) => void;
    addPostHandler: (post: Post) => void;
}

const initialPostContext: PostContextInterface = {
    initialPosts: false,
    posts: [],
    setInitialPostsHandler: () => {},
    addPostHandler: () => {},
};

export const PostContext =
    React.createContext<PostContextInterface>(initialPostContext);

export const PostsContextProvider: React.FC<{
    children: React.ReactNode;
}> = ({ children }) => {
    const [initialPosts, setInitialPosts] = useState<boolean>(false);
    const [posts, setPosts] = useState<Post[]>([]);

    const setInitialPostsHandler = (posts: Post[]) => {
        if (!initialPosts) {
            setPosts(posts);
            setInitialPosts(true);
        }
    };

    const addPostHandler = (post: Post) => {
        setPosts((prevState: Post[]) => [...prevState, post]);
    };

    return (
        <PostContext.Provider
            value={{
                initialPosts,
                posts,
                setInitialPostsHandler,
                addPostHandler,
            }}
        >
            {children}
        </PostContext.Provider>
    );
};
