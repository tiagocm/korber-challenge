import React, { useState } from "react";
import { useAuth0 } from "@auth0/auth0-react";

import { User } from "../models/user";

const initialUser: User = {
    userId: 0,
    name: "",
    email: "",
};

interface AuthContextProviderInterface {
    user: User;
    loginHandler: () => void;
    setCurrentUser: () => void;
    logoutHandler: () => void;
}

const initialAuthContext: AuthContextProviderInterface = {
    user: initialUser,
    loginHandler: () => {},
    setCurrentUser: () => {},
    logoutHandler: () => {},
};

export const AuthContext =
    React.createContext<AuthContextProviderInterface>(initialAuthContext);

export const AuthContextProvider: React.FC<{
    children: React.ReactNode;
}> = ({ children }) => {
    const [user, setUser] = useState<User>(initialUser);
    const { loginWithRedirect, logout } = useAuth0();

    const loginHandler = () => {
        loginWithRedirect();
    };

    const setCurrentUser = () => {
        setUser({
            userId: 1,
            name: "Cristiano Ronaldo",
            email: window.localStorage.getItem("currentEmail") || "",
        });
    };

    const logoutHandler = () => {
        setUser(initialUser);
        logout({ returnTo: window.location.origin });
    };

    return (
        <AuthContext.Provider
            value={{ user, loginHandler, setCurrentUser, logoutHandler }}
        >
            {children}
        </AuthContext.Provider>
    );
};
