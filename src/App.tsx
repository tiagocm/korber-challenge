import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";

import Welcome from "./layouts/Welcome";
import Login from "./layouts/Login";
import Callback from "./layouts/Callback";
import Home from "./layouts/Home";

import ProtectedRoute from "./api/ProtectedRoute";

interface CustomRoute {
    path: string;
    element: React.ReactElement;
}

const routes: CustomRoute[] = [
    {
        path: "/",
        element: <Welcome />,
    },
    {
        path: "/login",
        element: <Login />,
    },
    {
        path: "/callback",
        element: <Callback />,
    },
    {
        path: "/home",
        element: (
            <ProtectedRoute>
                <Home />
            </ProtectedRoute>
        ),
    },
];

function App() {
    return (
        <BrowserRouter>
            <Routes>
                {routes.map((route, index) => {
                    return (
                        <Route
                            key={index}
                            path={route.path}
                            element={route.element}
                        />
                    );
                })}
            </Routes>
        </BrowserRouter>
    );
}

export default App;
