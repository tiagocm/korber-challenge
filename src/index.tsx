import ReactDOM from "react-dom/client";
import "./index.css";

import { Auth0Provider } from "@auth0/auth0-react";
import { AuthContextProvider } from "./store/auth-provider";
import { EmailContextProvider } from "./store/email-provider";
import { PostsContextProvider } from "./store/posts-provider";

import App from "./App";

const root = ReactDOM.createRoot(
    document.getElementById("root") as HTMLElement
);
root.render(
    <Auth0Provider
        domain="fisio.eu.auth0.com"
        clientId="Z0NBrRVust481NCjWFe9OSgLvaGUKr15"
        redirectUri="http://localhost:3000/callback"
    >
        <AuthContextProvider>
            <EmailContextProvider>
                <PostsContextProvider>
                    <App />
                </PostsContextProvider>
            </EmailContextProvider>
        </AuthContextProvider>
    </Auth0Provider>
);
