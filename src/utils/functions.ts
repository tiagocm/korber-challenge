export const getRandomFromArray = (array: any[]): any =>
    array[Math.floor(Math.random() * array.length)];
