export const postImages: string[] = [
    "https://www.koerber.com/fileadmin/Media/Images/Startseite/Pharma-EN.jpg",
    "https://www.koerber.com/fileadmin/_processed_/5/2/csm_KOERBER_Lion-meets-Disco_01_9c4cbcae58.jpg",
    "https://www.koerber.com/fileadmin/_processed_/c/7/csm_SupplyChain_we_conquer_complexity_d0c1f0679e.png",
    "https://www.koerber.com/fileadmin/_processed_/f/7/csm_Gartner22_Screen_Image_600x400_64239f9cbf.png",
    "https://www.koerber.com/fileadmin/_processed_/e/f/csm_Tissue-EN_685027a5f0.jpg",
    "https://www.koerber.com/fileadmin/_processed_/2/9/csm_header-koerber-ag-machine-learning_b7bab3b736.jpg",
    "https://www.koerber.com/fileadmin/_processed_/e/f/csm_Tissue-EN_685027a5f0.jpg",
];
