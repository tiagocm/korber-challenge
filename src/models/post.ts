export default interface Post {
    id?: number;
    userId: number;
    title: string;
    body: string;
    img?: string;
}
