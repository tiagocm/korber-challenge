import React, { useEffect, useState, useContext } from "react";
import { PostContext } from "../../../store/posts-provider";

import { AuthContext } from "../../../store/auth-provider";
import Spinner from "../../Spinner";

import Card from "../../Card";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";

import { routes } from "../../../api/routes";

const ListPosts = () => {
    const authCtx = useContext(AuthContext);
    const postCtx = useContext(PostContext);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        fetch(routes.userPosts + authCtx.user.userId)
            .then((response) => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error();
                }
            })
            .then((data) => {
                postCtx.setInitialPostsHandler(data);
            })
            .catch((e) => console.error(e));

        // show the spinner for half a second
        setTimeout(() => setIsLoading(false), 500);
        // eslint-disable-next-line
    }, [authCtx.user.userId]);

    return (
        <div style={{ padding: "20px 100px" }}>
            <Box sx={{ flexGrow: 1 }}>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <h1>
                            {authCtx.user.name} posts ({postCtx.posts.length})
                        </h1>
                    </Grid>
                    {isLoading && <Spinner />}
                    {!isLoading &&
                        postCtx.posts.length !== 0 &&
                        postCtx.posts.map((post) => (
                            <Grid item xs={3}>
                                <Card post={post} />
                            </Grid>
                        ))}
                    {!isLoading && postCtx.posts.length === 0 && (
                        <Grid item xs={3}>
                            <h3>There are no posts yet</h3>
                        </Grid>
                    )}
                </Grid>
            </Box>
        </div>
    );
};

export default ListPosts;
