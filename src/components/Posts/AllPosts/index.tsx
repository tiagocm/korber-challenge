import React, { useEffect, useState } from "react";

import Spinner from "../../Spinner";

import Card from "../../Card";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";

import { routes } from "../../../api/routes";

const AllPosts = () => {
    const [posts, setPosts] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        fetch(routes.allPosts)
            .then((response) => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error();
                }
            })
            .then((data) => {
                setPosts(data);
            })
            .catch((e) => console.error(e));

        // show the spinner for half a second
        setTimeout(() => setIsLoading(false), 500);
    }, []);

    return (
        <div style={{ padding: "20px 100px" }}>
            <Box sx={{ flexGrow: 1 }}>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <h1>
                            All posts ({posts.length})
                        </h1>
                    </Grid>
                    {isLoading && <Spinner />}
                    {!isLoading &&
                        posts.length !== 0 &&
                        posts.map((post) => (
                            <Grid item xs={3}>
                                <Card post={post} />
                            </Grid>
                        ))}
                    {!isLoading && posts.length === 0 && (
                        <Grid item xs={3}>
                            <h3>There are no posts yet</h3>
                        </Grid>
                    )}
                </Grid>
            </Box>
        </div>
    );
};

export default AllPosts;
