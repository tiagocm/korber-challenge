import React, { useState, useContext } from "react";

import Toast from "../../Toast";

import { AuthContext } from "../../../store/auth-provider";
import { PostContext } from "../../../store/posts-provider";

import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import TextField from "@mui/material/TextField";

import classes from "./NewPost.module.css";

import { routes } from "../../../api/routes";
import Post from "../../../models/post";

const NewPost = () => {
    const authCtx = useContext(AuthContext);
    const postCtx = useContext(PostContext);

    const [postTitle, setPostTitle] = useState("");
    const [postBody, setPostBody] = useState("");
    const [errorMessage, setErrorMessage] = useState("");
    const [openToast, setOpenToast] = useState(false);

    const changePostTitleHandler = (
        event: React.ChangeEvent<HTMLInputElement>
    ) => {
        setPostTitle(event.target.value);
    };

    const changePostBodyHandler = (
        event: React.ChangeEvent<HTMLTextAreaElement>
    ) => {
        setPostBody(event.target.value);
    };

    const submitPostHandler = () => {
        if (!postTitle) {
            setErrorMessage("Title of the post can not be empty!");
            return;
        }

        if (!postBody) {
            setErrorMessage("Body of the post can not be empty!");
            return;
        }

        setErrorMessage("");

        const newPost: Post = {
            title: postTitle,
            body: postBody,
            userId: authCtx.user.userId,
        };

        fetch(routes.userPosts + authCtx.user.userId, {
            method: "POST",
            body: JSON.stringify(newPost),
            headers: {
                "Content-type": "application/json; charset=UTF-8",
            },
        })
            .then((response) => response.json())
            .then((data) => {
                postCtx.addPostHandler(data);
                setOpenToast(true);
                setPostTitle("");
                setPostBody("");
            });
    };

    const onCloseHandler = () => {
        setOpenToast(false);
    };

    return (
        <div className={classes.NewPost}>
            <Box sx={{ flexGrow: 1 }}>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <h1>Add new post</h1>
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            onChange={changePostTitleHandler}
                            value={postTitle}
                            id="post-title"
                            label="Post title"
                            variant="outlined"
                            fullWidth
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            onChange={changePostBodyHandler}
                            value={postBody}
                            id="post-body"
                            label="Post body"
                            multiline
                            rows={4}
                            variant="outlined"
                            fullWidth
                        />
                    </Grid>
                    <Grid item xs={2}>
                        <Button
                            style={{ backgroundColor: "#0160FF" }}
                            variant="contained"
                            onClick={submitPostHandler}
                        >
                            Add new post
                        </Button>
                    </Grid>
                    {errorMessage && (
                        <Grid item xs={10}>
                            <h3 style={{ textAlign: "right" }}>
                                {errorMessage}
                            </h3>
                        </Grid>
                    )}
                    <Toast open={openToast} onCloseHandler={onCloseHandler} />
                </Grid>
            </Box>
        </div>
    );
};

export default NewPost;
