import React, { useContext } from "react";
import { AuthContext } from "../../store/auth-provider";

import classes from "./Header.module.css";

interface HeaderProps {
    username: string;
    img: string;
}

const Header: React.FC<HeaderProps> = ({ img, username }) => {
    const authCtx = useContext(AuthContext);

    return (
        <header className={classes.Header}>
            <img
                src="https://www.theportugalnews.com/uploads/news/Cristiano_Ronaldo_2018__cropped_.jpg"
                alt={authCtx.user?.name}
            />
            <div className={classes.Username}>
                Welcome, {authCtx.user?.name}
            </div>
            <button onClick={() => authCtx.logoutHandler()}>
                <i className="fa-solid fa-arrow-right-from-bracket"></i>
            </button>
        </header>
    );
};

export default Header;
