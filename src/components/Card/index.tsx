import React from "react";

import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { CardActionArea } from "@mui/material";

import Post from "../../models/post";
import { postImages } from "../../constants/post-images";
import { getRandomFromArray } from "../../utils/functions";

const ActionAreaCard: React.FC<{ post: Post }> = ({ post }) => {
    return (
        <Card
            sx={{ maxWidth: 345 }}
            style={{ marginTop: "20px", height: "400px" }}
        >
            <CardActionArea>
                <CardMedia
                    component="img"
                    height="140"
                    image={getRandomFromArray(postImages)}
                    alt={post.title}
                />
                <CardContent>
                    <Typography gutterBottom variant="h6" component="div">
                        {post.title}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        {post.body}
                    </Typography>
                </CardContent>
            </CardActionArea>
        </Card>
    );
};

export default ActionAreaCard;
