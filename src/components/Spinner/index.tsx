import classes from "./Spinner.module.css";

const Spinner = () => {
    return (
        <div className={`centered ${classes.Spinner}`}>
            <div></div>
            <div></div>
        </div>
    );
};

export default Spinner;
