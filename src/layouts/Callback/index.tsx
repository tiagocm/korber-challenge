import { useContext } from "react";
import { Navigate } from "react-router-dom";

import { AuthContext } from "../../store/auth-provider";

const Callback = () => {
    const authCtx = useContext(AuthContext);

    authCtx.setCurrentUser();

    return <Navigate to="/home" />;
};

export default Callback;
