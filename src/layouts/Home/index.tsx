import { useState, useContext } from "react";
import { AuthContext } from "../../store/auth-provider";

import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Box from "@mui/material/Box";

import Header from "../../components/Header";

import AllPosts from "../../components/Posts/AllPosts";
import UserPosts from "../../components/Posts/UserPosts";
import NewPost from "../../components/Posts/NewPost";

import classes from "./Home.module.css";


interface TabPanelProps {
    children?: React.ReactNode;
    index: number;
    value: number;
}

function TabPanel(props: TabPanelProps) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && <>{children}</>}
        </div>
    );
}

function a11yProps(index: number) {
    return {
        id: `simple-tab-${index}`,
        "aria-controls": `simple-tabpanel-${index}`,
    };
}

const Home = () => {
    const [value, setValue] = useState(0);
    const authCtx = useContext(AuthContext);

    const handleChange = (event: React.SyntheticEvent, newValue: number) => {
        setValue(newValue);
    };

    return (
        <div className={classes.Home}>
            <Header username={authCtx.user?.name} img="" />
            <Box sx={{ width: "100%" }}>
                <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
                    <Tabs value={value} onChange={handleChange}>
                        <Tab label="All posts" {...a11yProps(0)} />
                        <Tab label="User posts" {...a11yProps(1)} />
                        <Tab label="New post" {...a11yProps(2)} />
                    </Tabs>
                </Box>
                <TabPanel value={value} index={0}>
                    <AllPosts />
                </TabPanel>
                <TabPanel value={value} index={1}>
                    <UserPosts />
                </TabPanel>
                <TabPanel value={value} index={2}>
                    <NewPost />
                </TabPanel>
            </Box>
        </div>
    );
};

export default Home;
