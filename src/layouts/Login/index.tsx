import { useContext, useRef } from "react";

import classes from "./Login.module.css";
import { EmailContext } from "../../store/email-provider";
import { AuthContext } from "../../store/auth-provider";

const Login = () => {
    const authCtx = useContext(AuthContext);

    const emailContext = useContext(EmailContext);
    const emailRef = useRef<HTMLInputElement>(null);

    const onChangeEmailInput = () => {
        emailContext.onChangeEmailInput(emailRef.current!.value);
    };

    const triggerAuth0Login = () => {
        if (emailContext.state.valid) {
            authCtx.loginHandler();
            return;
        }
        emailContext.onChangeEmailInput(emailRef.current!.value);
    };

    return (
        <div className={classes.Login}>
            <div className="centered">
                <h2>Körber Login</h2>
                {emailContext.state.touched && !emailContext.state.valid && (
                    <p>
                        <i className="fa-solid fa-triangle-exclamation"></i> The
                        entered email is not valid
                    </p>
                )}
                <input
                    ref={emailRef}
                    placeholder="Enter your email"
                    onChange={onChangeEmailInput}
                    value={emailContext.state.value}
                />
                <button onClick={triggerAuth0Login}>Login</button>
            </div>
        </div>
    );
};

export default Login;
