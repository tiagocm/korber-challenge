import { useNavigate } from "react-router-dom";
import classes from "./Welcome.module.css";
import korberLogo from "../../assets/korber-logo.svg";

const Welcome = () => {
    const navigate = useNavigate();

    const redirectHandler = () => {
        navigate("/login");
    };

    return (
        <div className={classes.Welcome}>
            <div className="centered">
                <img src={korberLogo} alt="Korber logo" />
                <button onClick={redirectHandler}>Welcome</button>
            </div>
        </div>
    );
};

export default Welcome;
