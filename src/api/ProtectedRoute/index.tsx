import React, { useContext } from "react";
import { Navigate } from "react-router-dom";

import { AuthContext } from "../../store/auth-provider";

const ProtectedRoute: React.FC<{
    children: React.ReactNode;
}> = ({ children }) => {
    const authCtx = useContext(AuthContext);

    if (authCtx.user.userId === 0) {
        // not logged in so redirect to login page with the return url
        return <Navigate to="/login" />;
    }

    // authorized so return child components
    return <>{children}</>;
};

export default ProtectedRoute;
