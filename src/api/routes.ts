const baseUrl = "https://jsonplaceholder.typicode.com";

export const routes = {
    allPosts: baseUrl + "/posts",
    userPosts: baseUrl + "/posts?userId=",
};
